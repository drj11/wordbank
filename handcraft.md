# Campward Ho! A Manual for Girl Scout Camps

From https://www.gutenberg.org/cache/epub/33767/pg33767-images.html#Page_98

# Handcrafts

The handcrafts are more numerous than your fingers and can be defined as anything that is done with the hands. It is possible to have almost any branch of the Fine Arts and the Applied Arts as dyeing, batik, stenciling, woodblock printing, pottery. Then there is basketry, weaving, rug-making, leather work, and metal work in copper, or jewelry in silver, woodcarving and carpentry. The first problem is: "Who will teach it?" The choice of what handcrafts you will have then, depends somewhat on whom you can secure to present them properly.

But closely allied is your second problem, "What can we afford?" Jewelry, metal work and leather are the most expensive. Pottery is fascinating, but you must have a kiln to finish the product.

Try to choose the crafts which will suit the capacities. It is better not to attempt jewelry at the outset.

Relating your craft work to the camp makes it doubly interesting. So much can be done in this way with carpentry which produces anything from docks and canoe paddles to furniture and toothbrush holders.

Delightful problems in the interior decoration of a camp living room can be worked out by combining the efforts of all the craft workers. The carpenters build the furniture; the weavers make rugs and materials; the dyers dip the materials and carry out the color scheme and other workers supply the accessories.

It is well to have an exhibition to look forward to for the end of the season when appointed judges decide upon the merit of the work.
