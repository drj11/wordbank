# Leipzig

extract from https://www.gutenberg.org/cache/epub/42048/pg42048-images.html#ar51

# Leipzig Commerce

In the trades of bookselling and publishing Leipzig occupies a unique position, not only taking the first place in Germany, but even surpassing London and Paris in the number and total value of its sales. There are upwards of nine hundred publishers and booksellers in the town, and about eleven thousand firms in other parts of Europe are represented here. Several hundred booksellers assemble in Leipzig every year, and settle their accounts at their own exchange (Buchhändler-Börse). Leipzig also contains about two hundred printing-works, some of great extent, and a corresponding number of type-foundries, binding-shops and other kindred industries.

The book trades give employment to over 15,000 persons, and since 1878 Leipzig has grown into an industrial town of the first rank. The iron and machinery trades employ 4500 persons; the textile industries, cotton and yarn spinning and hosiery, 6000; and the making of scientific and musical instruments, including pianos, 2650. Other industries include the manufacture of artificial flowers, wax-cloth, chemicals, ethereal oils and essences, beer, mineral waters, tobacco and cigars, lace, india-rubber wares, rush-work and paper, the preparation of furs and numerous other branches. These industries are mostly carried on in the suburbs of Plagwitz, Reudnitz, Lindenau, Gohlis, Eutritzsch, Konnewitz and the neighbouring town of Markranstädt.

# END
