# Test strings, organised by theme

## Sources

Karen Cheng has suggested testers for each letter,
marked with _KC_ here.

French pangram from http://www.fatrazie.com/jeux-de-mots/recreamots/280-heterogrammes-thematiques

Longer Czech pangram from
https://fediscience.org/@samweingamgee;
shorter Czech pangram (with "kůň") from https://scholar.social/@animalculum

Stahlmöbel advert is from https://fontsinuse.com/uses/10166/erga-stahlmo-bel-ad

Ento text with A̤ (A with DIERESIS BELOW) is from «Journeys to
the Planet Mars» by Sara Weiss
https://gutenberg.org/ebooks/61253

“Art of Lettering” is from an ATF advert: https://fontsinuse.com/uses/22943/bradley-his-book-vol-1-no-1

“Electricity” from a Use Electricity advert https://fontsinuse.com/uses/7426/use-electricity-advert-for-the-dublin-corpora

“BUTLER” wheel text from an 1896 advert: https://fontsinuse.com/uses/7025/the-1896-butler-bicycle

Irish examples of TIRONIAN SIGN ET from
https://stancarey.wordpress.com/2014/09/18/the-tironian-et-in-galway-ireland/
and https://archive.org/details/storiesfromkeati00keat/

List of Short Phrases from Stephen Cole, published on
Typographica https://library.typographica.org/short-phrases-for-typeface-specimens

„İlham Əliyev“ from a Christoph Koeberlin post.


## Schema

CSV file has 4 columns:

    tag,Script,Lang,text

- `tag`: intended to be useful for searching/selecting
- `Script`: the script system, for example, Latin
- `Lang`: IETF BCP 47 language code
- `text`: the word or other text

`Lang` in almost all cases will be a single BCP 47 language subtag;
most of the common languages are 2 letter codes.
The official registry is:
http://www.iana.org/assignments/language-subtag-registry/language-subtag-registry

Note:

- en English
- ca Catalan (and Valencian)
- es Spanish
- tr Turkish
- cy Welsh
- de German
- mi Maori
- lv Latvian
- lt Lithuanian
- mt Maltese
- fr French
- ro Romanian
- smi Sami Languagues (collection; DEPRECATED)

# END
