# Invention

From https://books.googleusercontent.com/books/content?req=AKW5QadNXvCSQA9txIHSm3hysWSqHwyNu81sDwUg7Oi7WW-MDVZzyFPwJFHwArnqJl2k8Qy5U4KUtXWiZknXmeVf-XA-aZY2zTvrD5WyUHgVHh1O_rGQcJkTVIGS9VCikLdwl63avshGaUXlHqm8k39-VOhWAX1m42bRhBP4htv0P-cGexWFlFqbHuDO5Hd0BxxhjfgrR97LgnZP2NUikmQLsuaE02wZUkj-kPdlLxmSUXHZATMfZsbHiTtRiu6IXIZ4DYaFB3Kni2BZM6gWlzvRqDNR8fEreg

# PREFACE

THE Invention of Printing has always been recognized
by educated men as a subject of importance : there is no
mechanical art, nor are there any of the fine arts, about whose
early history so many books have been written.
The subject is
as mysterious as it is inviting. There is an unusual degree
of obscurity about the origin of the first printed books and the
lives and works of the early printers. There are records and
traditions which cannot be reconciled of at least three distinct
inventions of printing. Its early history is entangled with a
controversy about rival inventors which has lasted for more
than three centuries, and is not yet fully determined.

In the management of this controversy, a subject intrinsic-
ally attractive has been made repulsive. The history of the
invention ofprinting has been written to please national pride.
German authors assert the claims of Gutenberg, and discredit
traditions about Coster. Dutch authors insist on the priority
of Coster, and charge Gutenberg with stealing the invention.
Partisans on each side say that their opponents have perverted
the records and suppressed the truth. The quarrel has spread.
English and French authors, who had no national prejudices to
gratify, and who should have considered the question without
passion, have wrangled over the subject with all the bitterness
of Germans or Hollanders. In this, as in other quarrels, there
are amusing features, but to the general reader the controversy
seems unfortunate and is certainly wearisome.

It is a greater misfortune that all the early chronicles of
printing were written in a dead language. Wolf’s collection
of Typographic Monuments, which includes nearly every paper
of value written before 1740, is in Latin ; the valuable books
of Meerman, Maittaire, and Schoepflin are also in Latin. To
the general reader these are sealed books: to the student, who
seeks exact knowledge of the methods of the first printers, they
are tiresome books. Written for the information of librarians
rather than of printers, it is but proper that these books should
devote the largest space to a review of the controversy or to a
description of early editions ; but it is strange that they should
so imperfectly describe the construction and appearance of early
types and the usages of the early printers. The mechanical
features of typography were, apparently, neglected as of little
importance, and beneath the dignity of history.
