# Queen of the Martin Catacombs

From https://www.gutenberg.org/cache/epub/63956/pg63956-images.html

# Queen Of The Martian Catacombs

For hours the hard-pressed beast had fled across the Martian desert with its dark rider. Now it was spent. It faltered and broke stride, and when the rider cursed and dug his heels into the scaly sides, the brute only turned its head and hissed at him. It stumbled on a few more paces into the lee of a sandhill, and there it stopped, crouching down in the dust.

The man dismounted. The creature's eyes burned like green lamps in the light of the little moons, and he knew that it was no use trying to urge it on. He looked back, the way he had come.

In the distance there were four black shadows grouped together in the barren emptiness. They were running fast. In a few minutes they would be upon him.

He stood still, thinking what he should do next. Ahead, far ahead, was a low ridge, and beyond the ridge lay Valkis and safety, but he could never make it now. Off to his right, a lonely tor stood up out of the blowing sand. There were tumbled rocks at its foot.

"They tried to run me down in the open," he thought. "But here, by the Nine Hells, they'll have to work for it!"

He moved then, running toward the tor with a lightness and speed incredible in anything but an animal or a savage. He was of Earth stock, built tall, and more massive than he looked by reason of his leanness. The desert wind was bitter cold, but he did not seem to notice it, though he wore only a ragged shirt of Venusian spider silk, open to the waist. His skin was almost as dark as his black hair, burned indelibly by years of exposure to some terrible sun. His eyes were startlingly light in colour, reflecting back the pale glow of the moons.

With the practised ease of a lizard he slid in among the loose and treacherous rocks. Finding a vantage point, where his back was protected by the tor itself, he crouched down.

After that he did not move, except to draw his gun. There was something eerie about his utter stillness, a quality of patience as unhuman as the patience of the rock that sheltered him.
