# Universal Declaration of Human Rights

Extracts from the Universal Declaration of Human Rights in
various languages.
From their translation project.

It may be better to use https://www.unicode.org/udhr/

For specimen purposes, the extracts should begin with the last
portion of the preamble, at the equivalent place where the
English version has "The General Assembly,"
(after "Now, therefore,").

## Technical notes

Romanian, in ron.txt, has a [historical encoding issue for the
letters S WITH COMMA BELOW and T WITH COMMA
BELOW](https://en.wikipedia.org/wiki/%C8%98).
As pasted from the UDHR website, the text was encoded using S
WITH CEDILLA and T WITH CEDILLA.
I have replaced these with their WITH COMMA forms.

# END
