# FASCISM!

From Army Talk Orientation Fact Sheet 64

#

“Fascism” is a word that’s been used a great deal
these last few years. We come across it in our news-
papers, we hear it in our newsreels, it comes up in
our bull session. We’ve heard about the cruelties of
fascsm, its terror, its conquest of country after coun-
try. We’ve heard of its concentration camps—like
Dachau in Germany and its tortute chambers—like
Maidanek in Poland. We’ve heard of its planned
mass murder of whole peoples—which scholars call
“genocide”.

Some of the things that have been done to people
by fascists seem too horrible to believe, especially to
Americans who believe in “live and let live.” Hard-
boiled American correspondents, formerly skeptical,
now believe because they have seen. (See page 6.)

We Americans have been fighting fascists for more
than three years. When Cecil Brown, one of the
leading war correspondents, came back from the battle
fronts, he went on a trip that took him into big cities
and small towns all over America. He talked and
listened to all kinds of people. He found that most
Americans are vague about just what fascism really
means. He found few Americans who were confident
they would recognize a fascist if they saw one.

And are we in uniform any more certain what
fascism is—where it came from—what made it strong?
Do we know how fascism leads men to do the the things
done to people at Maidanek? Do we know how it
leads them to attack helpless nations? Are maidaneks
and war invevitable results of fascism? Do all fascists
speak only German, Italian or Japanese—or do some
of them speak our language? Will militar victory
in this War automatically kill fascism? Or could
fascism rise in the United States after it’s been crushed
abroad? What can we do to prevent it?

